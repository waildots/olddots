#+STARTUP: overview 
* Interface Tweaks
** Startup, general settings
#+BEGIN_SRC emacs-lisp
;; Hide Scroll bar,menu bar, tool bar and startup message
(setq inhibit-startup-message t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

;; Line numbering
(global-display-line-numbers-mode)
(setq display-line-numbers-type 'relative)

;; disable line numbers for some modes
(dolist (mode '(term-mode-hook
  	  dired-mode-hook))
   (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; visual bell
;; Set up the visible bell
(setq visible-bell t)
(fset 'yes-or-no-p 'y-or-n-p)
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-M-u") 'universal-argument)
#+END_SRC
** Scrolling
#+BEGIN_SRC emacs-lisp
;;keep cursor at same position when scrolling
(setq scroll-preserve-screen-position 1)
;;scroll window up/down by one line
(global-set-key (kbd "M-n") (kbd "C-u 1 C-v"))
(global-set-key (kbd "M-p") (kbd "C-u 1 M-v"))
#+END_SRC

** Font
#+BEGIN_SRC emacs-lisp
(set-face-attribute 'default nil :font "Mononoki Nerd Font" :height 128)
#+END_SRC

** Theming & Aesthethic
#+BEGIN_SRC emacs-lisp
;; the great dracula theme
;;(use-package dracula-theme
;;  :init (load-theme 'dracula t))
(use-package doom-themes
  :init (load-theme 'doom-one t))

;; highlight the current line
(global-hl-line-mode t) ;; This highlights the current line in the buffer

;; always wrap text
(add-hook 'text-mode-hook 'visual-line-mode)

#+END_SRC
    
** Evil-mode
#+BEGIN_SRC emacs-lisp
(use-package evil
  :init 
  (setq evil-want-C-u-scroll t)
  (setq evil-respect-visual-line-mode t)
  :ensure t
  :config
  (evil-mode 1))

(define-key evil-ex-map "e" 'find-file)
(define-key evil-ex-map "W" 'save-buffer)

;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
#+END_SRC

** Modeline
#+BEGIN_SRC emacs-lisp
(use-package all-the-icons)

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :hook (after-init . doom-modeline-init)
  ;;:hook (doom-modeline-mode . size-indication-mode) ; filesize in modeline
  ;;:hook (doom-modeline-mode . column-number-mode)   ; cursor column in modeline
  :custom-face
  (mode-line ((t (:height 0.98))))
  (mode-line-inactive ((t (:height 0.98))))
  :custom
  (doom-modeline-height 18)
  (doom-modeline-bar-width 3)
  (doom-modeline-lsp t)
  (doom-modeline-github nil)
  (doom-modeline-mu4e nil)
  (doom-modeline-irc nil)
  (doom-modeline-minor-modes nil)
  (doom-modeline-persp-name nil)
  (doom-modeline-buffer-file-name-style 'relative-from-project)
  (doom-modeline-major-mode-icon nil)) 


;;;;; Extensions
;;(use-package anzu
;;  :after isearch-mode)
;;
;;(use-package evil-anzu
;;  :after evil-ex-start-search evil-ex-start-word-search evil-ex-search-activate-highlight
;;  :config (global-anzu-mode +1))

#+END_SRC

** load path
#+BEGIN_SRC emacs-lisp
;; Add my library path to load-path
(push "/home/wn/myemacs/lisp" load-path)
#+END_SRC

* Packages
** Try
#+BEGIN_SRC emacs-lisp
(use-package try
             :ensure t)
#+END_SRC

** Rainbow-delimiters
#+BEGIN_SRC emacs-lisp
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))
#+END_SRC

** Which-key
#+BEGIN_SRC emacs-lisp
(use-package which-key
             :ensure t
             :config (which-key-mode))
#+END_SRC

** Ace-window
#+BEGIN_SRC emacs-lisp
(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap-other-window] 'ace-window)
    ))
#+END_SRC

** Counsel
#+BEGIN_SRC emacs-lisp
(use-package counsel
  :ensure t
  :demand t
  :bind (("M-x" . counsel-M-x)
	   ("C-x b" . counsel-ibuffer)
	   ("C-x C-f" . counsel-find-file)
	   ("C-x d" . counsel-dired)
	   ("C-M-j" . counsel-switch-buffer)
	   ("C-M-l" . counsel-imenu)
	   :map minibuffer-local-map
	   ("C-r" . 'counsel-minibuffer-history)))
#+END_SRC

** Swiper
#+BEGIN_SRC emacs-lisp
(use-package swiper
  :ensure t
  :config
  (progn
    (ivy-mode)
    (setq ivy-use-virtual-buffers t)
    (setq enable-recursive-minibuffers t)
    ;; enable this if you want `swiper' to use it
    ;; (setq search-default-mode #'char-fold-to-regexp)
    (global-set-key "\C-s" 'swiper)
    (global-set-key (kbd "C-c C-r") 'ivy-resume)
    (global-set-key (kbd "<f6>") 'ivy-resume)
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-x C-f") 'counsel-find-file)
    (global-set-key (kbd "<f1> f") 'counsel-describe-function)
    (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
    (global-set-key (kbd "<f1> o") 'counsel-describe-symbol)
    (global-set-key (kbd "<f1> l") 'counsel-find-library)
    (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
    (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
    (global-set-key (kbd "C-c g") 'counsel-git)
    (global-set-key (kbd "C-c j") 'counsel-git-grep)
    (global-set-key (kbd "C-c k") 'counsel-ag)
    (global-set-key (kbd "C-x l") 'counsel-locate)
    (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
    (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
    ))
#+END_SRC

** Ivy-rich
#+BEGIN_SRC emacs-lisp
(use-package ivy-rich
  :ensure t
  :init
  (ivy-rich-mode 1))
#+END_SRC
      
** Company
#+BEGIN_SRC emacs-lisp
  (use-package company
    :ensure t
    :config
    (global-company-mode))
;;(use-package company
;;  :ensure t
;;  :config
;;  (progn
;;    (add-hook 'after-init-hook 'global-company-mode)
;;    ))
#+END_SRC

** Helpful
#+BEGIN_SRC emacs-lisp
(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . helpful-function)
  ([remap describe-symbol] . helpful-symbol)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-command] . helpful-command)
  ([remap describe-key] . helpful-key)) 
#+END_SRC

** Ido
#+BEGIN_SRC emacs-lisp
(setq indo-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
#+END_SRC

** Ibuffers
#+BEGIN_SRC emacs-lisp
(defalias 'list-buffers 'ibuffer)
#+END_SRC

** Dired
#+BEGIN_SRC emacs-lisp
(use-package all-the-icons-dired)

(use-package dired
  :ensure nil
  :defer 1
  :commands dired-jump
  :init
  (setq
        dired-listing-switches "-ahl -v --group-directories-first"
        dired-auto-revert-buffer t  ; don't prompt to revert; just do it
        dired-dwim-target t  ; suggest a target for moving/copying intelligently
        dired-hide-details-hide-symlink-targets nil
        ;; Always copy/delete recursively
        dired-recursive-copies  'always
        dired-recursive-deletes 'top
        ;; Ask whether destination dirs should get created when copying/removing files.
        dired-create-destination-dirs 'ask))


(use-package diredfl
  :hook (dired-mode . diredfl-mode))

;;  (map! :map dired-mode-map
;;        ;; Kill all dired buffers on q
;;        :ng "q" #'+dired/quit-all
;;      (:after dired
;;       (:map dired-mode-map
;;        :desc "Peep-dired image previews" "d p" #'peep-dired
;;        :desc "Dired view file" "d v" #'dired-view-file))

(use-package dired-open)
(use-package peep-dired)


;; Make 'h' and 'l' go back and forward in dired. Much faster to navigate the directory structure!
(evil-define-key 'normal dired-mode-map
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file) ; use dired-find-file instead if not using dired-open package
;; If peep-dired is enabled, you will get image previews as you go up/down with 'j' and 'k'
(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)
;; Get file icons in dired
;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "sxiv")
				("jpg" . "sxiv")
				("png" . "sxiv")
				("pdf" . "zathura")
				("epub" . "zathura")
				("csv" . "libreoffice")
				("mp3" . "mpv --speed=1.6 --ontop --no-border --no-terminal --force-window --autofit=380x380 --geometry=-15-60")
				("mkv" . "mpv --speed=1.6 --ontop --no-border --no-terminal --force-window --autofit=950x380 --geometry=-15-60")
				("webm" . "mpv --speed=1.6 --ontop --no-border --no-terminal --force-window --autofit=950x380 --geometry=-15-60")
				("mp4" . "mpv --speed=1.6 --ontop --no-border --no-terminal --force-window --autofit=950x380 --geometry=-15-60")))

#+END_SRC

** zen-mode
;;#+BEGIN_SRC emacs-lisp
;;(require 'zen-mode)
;;#+END_SRC

* Org Mode
** Asthetics
#+BEGIN_SRC emacs-lisp
(use-package org
  :config
  (setq org-ellipsis " ▾"))
#+END_SRC
** Evil-org for evil-mode integration
This is very fancy, it treats org-mode elements (i.e. sections, source blocks, tables)
as regular text objects ( 'ae' object ). Additionally, it allows reordering stuff using
M-j, M-k, M-l and M-h instead of the regular M-<arrow keys>.
Operators that deal with indentation ( < and > ) also indent headers.
#+BEGIN_SRC emacs-lisp
(use-package evil-org
  :ensure t
  :after (evil org)
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme '(navigation insert textobjects additional calendar))))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))
#+END_SRC

* To add 
- keybinding to kill current buffer
- keybinding to switch buffer
- make the scrolling while the cursor stays in the same position 
  

