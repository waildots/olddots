[[ -f ~/.bashrc ]] && . ~/.bashrc

# XDG Paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XINITRC="$XDG_CONFIG_HOME"/x11/xinitrc

# Fixing Paths
export LESSHISTFILE="-"
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export XINITRC="$XDG_CONFIG_HOME"/x11/xinitrc
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass

# Default programs:
export BROWSER="brave"
export EDITOR="nvim"
export TERMINAL="st" 
export READER="zathura"
export VIDEO="mpv"
export IMAGE="sxiv"


if [[ "$(tty)" = "/dev/tty1" ]]; then
    pgrep bspwm || pgrep dwm || xb
fi
